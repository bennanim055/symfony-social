<?php

namespace App\Controller;

use App\Entity\Post;
use App\Form\PostType;
use App\Repository\PostRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/post")
 */
class PostController extends AbstractController
{
    /**
     * @Route("/", name="post_index", methods={"GET"})
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     */
    public function index(PostRepository $postRepository): Response
    {
        return $this->render('post/index.html.twig', [
            'posts' => $postRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="post_new", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     */
    public function new(Request $request, EntityManagerInterface $entityManager): Response
    {
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            
            $file = $request->files->get('post')['image'];
            $uploads_directory = $this->getParameter('uploads_directory');
            $filename = md5(uniqid()).'.'.$file->guessExtension();
            $post->setImage($filename);
            $file->move($uploads_directory,$filename);

            $post->setUserPost($this->getUser());
            $entityManager->persist($post);
            $entityManager->flush();
            return $this->redirectToRoute('post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('post/new.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }
    

    /**
     * @Route("/{id}", name="post_show", methods={"GET"})
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     */
    public function show(Post $post): Response
    {
        return $this->render('post/show.html.twig', [
            'post' => $post,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="post_edit", methods={"GET", "POST"})
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     */
    public function edit(Request $request, Post $post, EntityManagerInterface $entityManager): Response
    {

        if ($post->getUserPost() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }else{

        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            
            $file = $request->files->get('post')['image'];
            $uploads_directory = $this->getParameter('uploads_directory');
            $filename = md5(uniqid()).'.'.$file->guessExtension();
            $post->setImage($filename);
            $file->move($uploads_directory,$filename);


            $entityManager->flush();

            return $this->redirectToRoute('post_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->render('post/edit.html.twig', [
            'post' => $post,
            'form' => $form->createView(),
        ]);
    }
    }

    /**
     * @Route("/{id}", name="post_delete", methods={"POST"})
     * @IsGranted("ROLE_USER", message="No access! Get out!")
     */
    public function delete(Request $request, Post $post, EntityManagerInterface $entityManager): Response
    {
        if ($post->getUserPost() !== $this->getUser()) {
            throw $this->createAccessDeniedException();
        }else{
        if ($this->isCsrfTokenValid('delete'.$post->getId(), $request->request->get('_token'))) {
            $entityManager->remove($post);
            $entityManager->flush();
        }}

        return $this->redirectToRoute('post_index', [], Response::HTTP_SEE_OTHER);
    }
}
