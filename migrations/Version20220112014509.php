<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220112014509 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post DROP INDEX UNIQ_5A8A6C8D13841D26, ADD INDEX IDX_5A8A6C8D13841D26 (user_post_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE post DROP INDEX IDX_5A8A6C8D13841D26, ADD UNIQUE INDEX UNIQ_5A8A6C8D13841D26 (user_post_id)');
    }
}
